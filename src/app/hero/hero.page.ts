import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.page.html',
  styleUrls: ['./hero.page.scss'],
})
export class HeroPage implements OnInit {

  constructor( public activateRoute:ActivatedRoute) { }

  ngOnInit() {

    let dataRecv = this.activateRoute.snapshot.paramMap.get('name')
    let dataMovie = this.activateRoute.snapshot.paramMap.get('movie')
    let dataAll = this.activateRoute.snapshot.paramMap.get
    console.log(dataRecv)
    console.log(dataMovie)
    console.log(dataAll)
  }

}
